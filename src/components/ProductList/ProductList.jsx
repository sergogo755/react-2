import React from "react";
import Card from "../Card/Card";
import Button from "../Button/Button";
import Modal from "../Modal/Modal"

class ProductList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            productsToShow: [],
            productsToCart: [],
            favoriteProducts: localStorage.getItem("favorites") || [],
            cardToCart: "",
        }
    }

    openModal = id => {
        this.setState({
            cardToCart: id
        })
    }

    closeModal = () => {
        this.setState({
            cardToCart: ""
        })
    }

    addToCart = () => {
        if (!this.state.productsToCart.includes(this.state.cardToCart)) {
            this.setState({
                productsToCart: [...this.state.productsToCart, this.state.cardToCart]
            })
        }

        this.closeModal();
    }

    
    addToFavorites = id => {
        this.setState({
            favoriteProducts: [...this.state.favoriteProducts, id]
        })
    }

    removeFromFavorites = id => {
        const newFavorites = this.state.favoriteProducts.filter(product => {
            return product !== id;
        })

        this.setState({
            favoriteProducts: newFavorites
        })

    }  
    componentDidMount() {
        if (typeof (this.state.favoriteProducts) === "string") {
            this.setState({
                favoriteProducts: this.state.favoriteProducts.split(",")
            })
        }

        fetch("./products.json").then(response => response.json()).then(products => {
            this.setState({
                productsToShow: products
            })
        }).catch(() => {
            console.log(":((");
        })
    }

    render() {
        { this.state.productsToCart.length > 0 && localStorage.setItem("cart", this.state.productsToCart) }
        { this.state.favoriteProducts.length > 0 ? localStorage.setItem("favorites", this.state.favoriteProducts) : localStorage.removeItem("favorites") }

        return (
            <>
                {
                    this.state.productsToShow.map(product => {
                        return <Card key={product.id}
                            id={product.id}
                            image={product.img}
                            name={product.name}
                            color={product.color}
                            code={product.code}
                            price={product.price}
                            removeFromFavorites={this.removeFromFavorites}
                            favoriteProducts={this.state.favoriteProducts}
                            openModal={this.openModal}
                            addToFavorites={this.addToFavorites} />
                    })
                }

                {
                    this.state.cardToCart !== "" && <>
                        <div onClick={this.closeModal} className="blackback"></div>
                        <Modal header="Add this to Basket?"
                            text="Go ahead.There are a lot of tea,just click OK"
                            color="#7fcd51"
                            closeModal={this.closeModal}
                            actions={<div className="modal-btns">
                                <Button text="Ok" func={this.addToCart} color="orange"></Button>
                                <Button text="Cancel" func={this.closeModal} color="darkgreen"></Button>
                            </div>}
                        />
                    </>
                }
            </>
        )
    }
}

export default ProductList;