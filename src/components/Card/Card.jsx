import React from "react";
import PropTypes from "prop-types";
import "./Card.scss";
import Button from "../Button/Button.jsx";

class Card extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isFavorite: false,
        }

    }

    openModal = () => {
        const { openModal, id } = this.props;
        openModal(id);
    }
    
    componentDidMount() {
        const { favoriteProducts, id } = this.props;

        favoriteProducts.forEach(product => {
            if (product === id) {
                this.setState({
                    isFavorite: true
                })
            }
        })
    }

    toggleFavorites = () => {
        const { addToFavorites, removeFromFavorites, id } = this.props;
        if (this.state.isFavorite === false) {
            this.setState({
                isFavorite: true
            })
            addToFavorites(id);
        } else {
            this.setState({
                isFavorite: false
            })
            removeFromFavorites(id)
        }
    }

    render() {
        const { image, name, code, price, color } = this.props;

        return (
            <div className="card">
                <img src={image} alt="#" />
                <div className="wrapper">
                    <h1>{color} {name}</h1>
                </div>
                <div className="wrapper">
                    <span className="description">50 Грамм:</span>
                    <span className="description"> {price}{code}</span>
                </div>
                <div className="wrapper">
                    <svg onClick={this.toggleFavorites} xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill={this.state.isFavorite ? "gold" : "#D3D3D3"}><path d="M0 0h24v24H0V0z" fill="none"/><path d="M12 21.35l-1.45-1.32C5.4 15.36 2 12.28 2 8.5 2 5.42 4.42 3 7.5 3c1.74 0 3.41.81 4.5 2.09C13.09 3.81 14.76 3 16.5 3 19.58 3 22 5.42 22 8.5c0 3.78-3.4 6.86-8.55 11.54L12 21.35z"/></svg>
                    <Button color="black" text="Add to cart" func={this.openModal} />
                </div>
            </div>
        )
    }
}

Card.propTypes = {
    id: PropTypes.string.isRequired,
    openModal: PropTypes.func.isRequired,
    addToFavorites: PropTypes.func.isRequired,
    removeFromFavorites: PropTypes.func.isRequired,
    image: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    code: PropTypes.string.isRequired,
    price: PropTypes.string.isRequired,
    color: PropTypes.string,
}

Card.defaultProps = {
    color: "#b12525",
    name: "Some Tea",
    price:"Not Available"
}

export default Card;