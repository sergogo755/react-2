import React from "react";
import ProductList from "./components/ProductList/ProductList";
import './App.scss';


class App extends React.Component {
  render() {
    return ( 
      <ProductList />
    )
  }
}

export default App;